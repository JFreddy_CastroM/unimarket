package co.edu.uniquindio.test;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.UsingDataSet;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.jboss.arquillian.transaction.api.annotation.Transactional;

import co.edu.uniquindio.dto.CompraDTO;
import co.edu.uniquindio.dto.ProductoDTO;
import co.edu.uniquindio.dto.UsuarioDTO;
import co.edu.uniquindio.persistencia.Calificacion;
import co.edu.uniquindio.persistencia.Comentario;
import co.edu.uniquindio.persistencia.Compra;
import co.edu.uniquindio.persistencia.Favoritos;
import co.edu.uniquindio.persistencia.MetodoPago;
import co.edu.uniquindio.persistencia.Persona;
import co.edu.uniquindio.persistencia.Producto;
import co.edu.uniquindio.persistencia.Usuario;

@RunWith(Arquillian.class)
public class ModeloTest {

	@PersistenceContext
	private EntityManager entityManager;

	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap.create(WebArchive.class, "test.war").addPackage(Persona.class.getPackage())
				.addAsResource("persistenceForTest.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "producto.json" })
	public void probarProducto() {

		Producto prod = entityManager.find(Producto.class, "6");

		Assert.assertEquals("Celular Samsung", prod.getNombre());
		;

	}

	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "producto.json" })
	public void probarProductoAgregar() throws ParseException {

		Producto u = new Producto();
		u.setNombre("cartuchera");
		u.setCodigo("4");
		u.setDisponibilidad(35);
		u.setDescripcion("Cartuchera rosada");
		u.setFechaLimite(new SimpleDateFormat("yyyy-MM-dd").parse("2019-10-09"));
		u.getImagenes().add("456789");
		u.setPrecio(2300000);
		entityManager.persist(u);

		Producto usu = entityManager.find(Producto.class, u.getCodigo());

		Assert.assertEquals(u, usu);

	}

	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "producto.json" })
	public void probarProductoEliminar() {

		Producto u = entityManager.find(Producto.class, "3");
		Assert.assertTrue(u.getPrecio() == 500000);

		entityManager.remove(u);

		Producto us = entityManager.find(Producto.class, "3");
		Assert.assertNull(us);
	}

	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "producto.json" })
	public void probarProductoActualizar() {
		Producto u = entityManager.find(Producto.class, "3");
		Assert.assertTrue(u.getPrecio() == 500000);

		u.setPrecio(800000);

		entityManager.merge(u);

		Producto us = entityManager.find(Producto.class, "3");
		Assert.assertTrue(us.getPrecio() == 800000);

	}

	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json" })
	public void probarUsuario() {

		Persona user = entityManager.find(Persona.class, "5");

		Assert.assertEquals("Pepito", user.getNombre());
		;

	}

	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json" })
	public void probarUsuarioAgregar() {

		Usuario u = new Usuario();
		u.setCedula("4");
		u.setNombre("Stefanny");
		u.setApellido("Roman");
		u.setEmail("s@mail.com");
		u.setContrasena("1234");
		u.setDireccion("12344");
		u.setTelefono("123466");

		entityManager.persist(u);

		Usuario usu = entityManager.find(Usuario.class, u.getCedula());

		Assert.assertEquals(u, usu);

	}

	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json" })
	public void probarUsuarioEliminar() {

		Persona u = entityManager.find(Persona.class, "98765");
		Assert.assertTrue(u.getDireccion().equals("bbbbbbbbb"));

		entityManager.remove(u);

		Persona us = entityManager.find(Persona.class, "98765");
		Assert.assertNull(us);
	}

	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json" })
	public void probarUsuarioActualizar() {
		Persona u = entityManager.find(Persona.class, "98765");
		Assert.assertTrue(u.getDireccion().equals("bbbbbbbbb"));

		u.setDireccion("asd2457");

		entityManager.merge(u);

		Persona us = entityManager.find(Persona.class, "98765");
		Assert.assertTrue(us.getDireccion().equals("asd2457"));

	}

	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "comentario.json" })
	public void probarComentario() {

		Comentario com = entityManager.find(Comentario.class, "3");

		Assert.assertNotNull(com);

	}

	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "comentario.json" })
	public void probarComentarioAgregar() {
		Comentario comen = new Comentario();
		comen.setComentario("producto en mal estado");
		comen.setIdComentario("6");
		comen.setProducto(new Producto());
		comen.setUsuario(new Usuario());

		entityManager.persist(comen);

		Comentario c = entityManager.find(Comentario.class, comen.getIdComentario());

		Assert.assertEquals(comen, c);
	}

	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "comentario.json" })
	public void probarComentarioEliminar() {

		Comentario com = entityManager.find(Comentario.class, "3");
		Assert.assertTrue(com.getComentario().equals("Producto en mal estado"));

		entityManager.remove(com);

		Persona c = entityManager.find(Persona.class, "Producto en mal estado");
		Assert.assertNull(c);
	}

	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "comentario.json" })
	public void probarComentarioActualizar() {
		Comentario com = entityManager.find(Comentario.class, "3");
		Assert.assertTrue(com.getComentario().equals("Producto en mal estado"));

		com.setComentario("El producto fue de mi agrado");

		entityManager.merge(com);

		Comentario c = entityManager.find(Comentario.class, "3");
		Assert.assertTrue(c.getComentario().equals("El producto fue de mi agrado"));

	}

	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "favoritos.json" })
	public void probarFavoritos() {

		Favoritos fav = entityManager.find(Favoritos.class, "2");

		Assert.assertNotNull(fav);

	}

	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "favoritos.json" })
	public void probarFavoritosAgregar() {
		Favoritos fav = new Favoritos();
		fav.setIdFavorito("7");
		fav.setProducto(new Producto());
		fav.setUsuario(new Usuario());

		entityManager.persist(fav);

		Favoritos c = entityManager.find(Favoritos.class, fav.getIdFavorito());

		Assert.assertEquals(fav, c);

	}

	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "favoritos.json" })
	public void probarFavoritosEliminar() {
		Favoritos fav = entityManager.find(Favoritos.class, "2");
		Assert.assertTrue(fav.getIdFavorito().equals("2"));

		entityManager.remove(fav);

		Favoritos f = entityManager.find(Favoritos.class, fav.getIdFavorito());
		Assert.assertNull(f);

	}

	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "favoritos.json" })
	public void probarFavoritosActualizar() {
		Favoritos com = entityManager.find(Favoritos.class, "2");
		Assert.assertTrue(com.getIdFavorito().equals("2"));

		com.setIdFavorito("9");

		entityManager.merge(com);

		Favoritos c = entityManager.find(Favoritos.class, "2");
		Assert.assertTrue(c.getIdFavorito().equals("9"));

	}
	
	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "calificacion.json" })
	public void probarCalificacion() {

		Calificacion cal = entityManager.find(Calificacion.class, "1");

		Assert.assertNotNull(cal);

	}
	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "calificacion.json" })
	public void probarCalificaionAgregar() {
		
		Calificacion cal = new Calificacion();
		cal.setIdCalificacion("8");
		cal.setCalificacion(9.8);
		cal.setProducto(new Producto());
		cal.setUsuarios(new Usuario());
		
		entityManager.persist(cal);
		
		Calificacion c = entityManager.find(Calificacion.class, cal.getIdCalificacion());

		Assert.assertEquals(cal, c);
		
	}
	
	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "calificacion.json" })
	public void probarCalificaionEliminar() {
		Calificacion cal = entityManager.find(Calificacion.class, "1");
		Assert.assertTrue(cal.getCalificacion()==5.4);
		
		entityManager.remove(cal);
		
		Calificacion c = entityManager.find(Calificacion.class, "1");
		Assert.assertNull(c);
		
	}
	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "calificacion.json" })
	public void probarCalificaionActualizar() {
		Calificacion cal = entityManager.find(Calificacion.class, "1");
		Assert.assertTrue(cal.getCalificacion()==5.4);

		cal.setCalificacion(8.5);
		
		entityManager.merge(cal);
		
		Calificacion c = entityManager.find(Calificacion.class, "1");
		Assert.assertTrue(c.getCalificacion()==8.5);
	}
	
	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "compra.json" })
	public void probarCompra() {
		Compra compra = entityManager.find(Compra.class, "2");
		
		Assert.assertNotNull(compra);
		
	}
	
	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "compra.json" })
	public void crearCompra() {
		Compra compra = new Compra();
		
		compra.setFechaCompra(new Date());
		compra.setIdCompra("4");
		compra.setMetodoPago(MetodoPago.TARJETA);
		
		entityManager.persist(compra);
		
		Compra c = entityManager.find(Compra.class, compra.getIdCompra());
		
		Assert.assertEquals(compra, c);
	}
	
	
	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "compra.json" })
	public void eliminarCompra() {
		Compra compra = entityManager.find(Compra.class, "2");
		
		entityManager.remove(compra);
		
		Compra c = entityManager.find(Compra.class, "2");
		
		Assert.assertNull(c);
	}
	
	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "compra.json" })
	public void actualizarCompra() {
		Compra compra = entityManager.find(Compra.class, "1");
		
		compra.setMetodoPago(MetodoPago.CONTADO);
		
		entityManager.merge(compra);
		
		Compra c = entityManager.find(Compra.class, "1");
		
		Assert.assertEquals(c.getMetodoPago(), compra.getMetodoPago());
	}
	
	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "compra.json" })
	public void listarCompraFecha() {
		
		Date fecha = new Date(119, 8, 30);
		
		Query q = entityManager.createNamedQuery(Compra.TODAS_COMPRAS);
		q.setParameter("fechaCompra", fecha);
		
		List l = q.getResultList();
		
		System.out.println(fecha);
		System.out.println(l);
		
	}
	
	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "compra.json" })
	public void listarCompraFechaDTO() {
		
		Date fecha = new Date(119, 8, 30);
		
		TypedQuery<CompraDTO> q = entityManager.createNamedQuery(Compra.TODAS_COMPRAS_DTO, CompraDTO.class);
		q.setParameter("fechaCompra", fecha);
		
		List<CompraDTO> l = q.getResultList();
		
		System.out.println(l);
		
	}
	
	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "compra.json" })
	public void listarProductos() {
		
		TypedQuery<Producto> q = entityManager.createNamedQuery(Producto.TODOS_PRODUCTOS, Producto.class);
		
		List<Producto> l = q.getResultList();
		
		System.out.println(l);
		
	}
	
	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "compra.json" })
	public void listarProductosDTO() {
		
		TypedQuery<ProductoDTO> q = entityManager.createNamedQuery(Producto.TODOS_PRODUCTOS_DTO, ProductoDTO.class);
		
		List<ProductoDTO> l = q.getResultList();
		
		System.out.println(l);
		
	}
	
	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "compra.json" })
	public void contarComprasUnicas() {
		
		Query q = entityManager.createNamedQuery(Compra.COMPRAS_UNICAS);
		
		List l = q.getResultList();
		
		System.out.println(l);
		
	}
	
	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "compra.json" })
	public void contarProductosTipo() {
		
		Query q = entityManager.createNamedQuery(Producto.CONTAR_POR_TIPO);
		
		List l = q.getResultList();
		
		System.out.println(l);
		
	}
	
	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "compra.json", "comentario.json" })
	public void listarProductosSinComentarios() {
		
		TypedQuery<Producto> q = entityManager.createNamedQuery(Producto.PRODUCTOS_SIN_COMENTARIOS, Producto.class);
		 
		List<Producto> l = q.getResultList();
		
		System.out.println(l);
		
	}
	
	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "compra.json", "comentario.json" })
	public void listarGmail() {
		
		TypedQuery<Usuario> q = entityManager.createNamedQuery(Usuario.USUARIOS_GMAIL, Usuario.class);
		
		List<Usuario> l = q.getResultList();
		
		System.out.println(l);
		
	}
	
	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "compra.json", "comentario.json" })
	public void listarProductosPorUsuario(){
		
		TypedQuery<UsuarioDTO > q = entityManager.createNamedQuery(Producto.PRODUCTOS_POR_USUARIO, UsuarioDTO.class);
		
		List<UsuarioDTO> l = q.getResultList();
		
		System.out.println(l);
		
	}
	
	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "compra.json", "comentario.json" })
	public void listarRegistroPorTipoMax(){
		Query q = entityManager.createNamedQuery(Producto.REGISTROS_POR_TIPO);
		
		List l=q.getResultList();
		
		System.out.println(l);
		
	}
	
	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "compra.json", "comentario.json" })
	public void listarProductoMasCostoso(){
		TypedQuery<Producto> q = entityManager.createNamedQuery(Producto.PRODUCTO_MAS_COSTOSO, Producto.class);
		
		List<Producto> l = q.getResultList();
		
		System.out.println(l);
		
	}

	@Test
	public void crearTablas() {

	}
}

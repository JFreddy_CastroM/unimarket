package co.edu.uniquindio.test;

import javax.ejb.EJB;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.UsingDataSet;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import Excepciones.CedulaRepetidaException;
import Excepciones.EmailRepetidoException;
import Excepciones.ProductoRepetidoException;

import org.jboss.arquillian.transaction.api.annotation.Transactional;

import co.edu.unimarket.ejb.NegocioEJB;
import co.edu.uniquindio.persistencia.Comentario;
import co.edu.uniquindio.persistencia.Persona;
import co.edu.uniquindio.persistencia.Producto;
import co.edu.uniquindio.persistencia.Usuario;

@RunWith(Arquillian.class)
public class NegocioTest {
	@EJB
	private NegocioEJB negocioEJB;

	@Deployment
	public static Archive<?> createDeploymentPackage() {
		return ShrinkWrap.create(JavaArchive.class).addClass(NegocioEJB.class)

				.addPackage(Producto.class.getPackage())
				.addAsResource("persistenceForTest.xml", "META-INF/persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");

	}

	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "compra.json" })
	public void probarAutenticacion() {
		Persona u = negocioEJB.autenticarUsuario("estebandido@email.co", "2899");

		Assert.assertNull(u);
	}

	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "compra.json" })
	public void probarBuscarEmail() {
		Persona u = negocioEJB.buscarUsuario("jfcm@email.co");
		
		System.out.println("Lista: " + u);
	}
	
	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "compra.json" })
	public void probarAutenticarUsuario() {
		Persona p = negocioEJB.autenticarUsuario("estebandido@email.co", "1234567890");
		
		System.out.println(p);
	}

	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "compra.json" })
	public void probarListarProductosDisponibles() {
		List<Producto> productos = negocioEJB.listarProductosDisponibles();
		
		System.out.println(productos);
	}

	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "compra.json" })
	public void probarListarComentarios() {
		
		List<Comentario> comentarios = negocioEJB.listarComentarios("6");
		
		System.out.println(comentarios);
	}

	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "compra.json" })
	public void probarCrearProducto() throws ParseException, ProductoRepetidoException {

		Producto u = new Producto();
		u.setNombre("cartuchera");
		u.setCodigo("4");
		u.setDisponibilidad(35);
		u.setDescripcion("Cartuchera rosada");
		u.setFechaLimite(new SimpleDateFormat("yyyy-MM-dd").parse("2019-10-09"));
		u.getImagenes().add("456789");
		u.setPrecio(2300000);
		
		negocioEJB.crearProducto(u);
		
		System.out.println(u);
	}

	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "compra.json" })
	public void probarRegistrarUsuario() throws CedulaRepetidaException, EmailRepetidoException {

		Usuario u = new Usuario();
		u.setCedula("4");
		u.setNombre("Stefanny");
		u.setApellido("Roman");
		u.setEmail("s@mail.com");
		u.setContrasena("1234");
		u.setDireccion("12344");
		u.setTelefono("123466");
		
		negocioEJB.registrarUsuario(u);
		
		System.out.println(u);
	}

	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "compra.json" })
	public void probarEditarProducto() throws ParseException {
		Producto u = new Producto();
		u.setNombre("cartuchera");
		u.setCodigo("4");
		u.setDisponibilidad(35);
		u.setDescripcion("Cartuchera");
		u.setFechaLimite(new SimpleDateFormat("yyyy-MM-dd").parse("2019-10-09"));
		u.getImagenes().add("456789");
		u.setPrecio(2300000);

		Producto p = negocioEJB.editarProducto(u);
	}

	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "compra.json" })
	public void probarBuscarUsuarioEmail() {
		Persona u = negocioEJB.buscarUsuario("estebandido@email.co");
		
		System.out.println(u);
	}

	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "compra.json" })
	public void probarListarUsuarios() {
		List<Usuario> usuarios = negocioEJB.listarUsuarios();
		
		System.out.println(usuarios);
	}

	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "compra.json" })
	public void probarBorrarUsuario() {
		negocioEJB.borrarUsuario("2899");
	}

	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "compra.json" })
	public void probarEditarUsuario() {
		Usuario u = new Usuario();
		u.setCedula("4");
		u.setNombre("Stefanny");
		u.setApellido("Roman");
		u.setEmail("sr@mail.com");
		u.setContrasena("1234");
		u.setDireccion("12344");
		u.setTelefono("123466");
		
		Usuario usuario = negocioEJB.editarUsuario(u);
	}

	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "compra.json" })
	public void probarBuscarProducto() {

		Producto p = negocioEJB.buscarProducto("2");
		
		System.out.println(p);
	}

	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "compra.json" })
	public void probarEliminarProducto() {
		
		negocioEJB.eliminarProducto("2");
		
	}

	@Test
	@Transactional(value = TransactionMode.ROLLBACK)
	@UsingDataSet({ "persona.json", "producto.json", "compra.json" })
	public void probarListarProductos() {
		
		List<Producto> productos = negocioEJB.listarProductos();
		
		System.out.println(productos);
	}
}

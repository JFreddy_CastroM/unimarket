package co.edu.uniquindio.persistencia;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Calificacion
 *
 */
@Entity
public class Calificacion implements Serializable {

	@Id
	private String idCalificacion;
	
	private double calificacion;

	@ManyToOne
	private Usuario usuarios;

	@ManyToOne
	private Producto producto;

	private static final long serialVersionUID = 1L;

	public Calificacion() {
		super();
	}

	public double getCalificacion() {
		return this.calificacion;
	}

	public void setCalificacion(double calificacion) {
		this.calificacion = calificacion;
	}

	public String getIdCalificacion() {
		return idCalificacion;
	}

	public void setIdCalificacion(String idCalificacion) {
		this.idCalificacion = idCalificacion;
	}

	public Usuario getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(Usuario usuarios) {
		this.usuarios = usuarios;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	@Override
	public String toString() {
		return "Calificacion [idCalificacion=" + idCalificacion + ", calificacion=" + calificacion + ", usuarios="
				+ usuarios + ", producto=" + producto + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idCalificacion == null) ? 0 : idCalificacion.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Calificacion other = (Calificacion) obj;
		if (idCalificacion == null) {
			if (other.idCalificacion != null)
				return false;
		} else if (!idCalificacion.equals(other.idCalificacion))
			return false;
		return true;
	}

	

	



}

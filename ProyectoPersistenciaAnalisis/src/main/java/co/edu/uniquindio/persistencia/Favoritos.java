package co.edu.uniquindio.persistencia;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Favoritos
 *
 */
@Entity

public class Favoritos implements Serializable {

	@Id
	private String idFavorito;
	

	@ManyToOne
	private Producto producto;

	@ManyToOne
	private Usuario usuario;

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idFavorito == null) ? 0 : idFavorito.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Favoritos other = (Favoritos) obj;
		if (idFavorito == null) {
			if (other.idFavorito != null)
				return false;
		} else if (!idFavorito.equals(other.idFavorito))
			return false;
		return true;
	}

	
	private static final long serialVersionUID = 1L;

	public Favoritos() {
		super();
	}

	@Override
	public String toString() {
		return "Favoritos [idFavorito=" + idFavorito + ", producto=" + producto + ", usuario=" + usuario + "]";
	}

	public String getIdFavorito() {
		return idFavorito;
	}

	public void setIdFavorito(String idFavorito) {
		this.idFavorito = idFavorito;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	

}

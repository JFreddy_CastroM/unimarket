package co.edu.uniquindio.persistencia;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Usuario
 *
 */

@Entity
@DiscriminatorValue("Usuario")
@NamedQueries({
	@NamedQuery(name = Usuario.USUARIOS_GMAIL, query = "select u from Usuario u where u.email like '%gmail%'"),
	@NamedQuery(name = Usuario.LISTAR_USUARIOS, query = "select u from Usuario u"),
	@NamedQuery(name = Usuario.BUSCAR_USUARIO_CEDULA, query = "select u from Usuario u where u.cedula = :cedula")	
})
public class Usuario extends Persona implements Serializable {

	@OneToMany
	private List<Calificacion> calificaciones;

	@OneToMany
	private List<Compra> compras;

	@OneToMany
	private List<Comentario> comentarios;

	@OneToMany
	private List<Favoritos> favoritos;
	
	@OneToMany
	private List<Producto> productosPublicados;

	public static final String USUARIOS_GMAIL = "USUARIOS_GMAIL";
	
	public static final String LISTAR_USUARIOS = "LISTAR_USUARIOS";
	
	public static final String BUSCAR_USUARIO_CEDULA = "BUSCAR_USUARIO_CEDULA";

	private static final long serialVersionUID = 1L;

	public Usuario() {
		super();
	}

	public List<Calificacion> getCalificaciones() {
		return calificaciones;
	}

	public void setCalificaciones(List<Calificacion> calificaciones) {
		this.calificaciones = calificaciones;
	}

	public List<Compra> getCompras() {
		return compras;
	}

	public void setCompras(List<Compra> compras) {
		this.compras = compras;
	}

	public List<Comentario> getComentarios() {
		return comentarios;
	}

	public void setComentarios(List<Comentario> comentarios) {
		this.comentarios = comentarios;
	}

	public List<Favoritos> getFavoritos() {
		return favoritos;
	}

	public void setFavoritos(List<Favoritos> favoritos) {
		this.favoritos = favoritos;
	}
	
	


}

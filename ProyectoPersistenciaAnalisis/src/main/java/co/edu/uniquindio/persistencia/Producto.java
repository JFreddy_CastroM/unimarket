package co.edu.uniquindio.persistencia;

import java.awt.Image;
import java.io.Serializable;
import java.lang.String;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Producto
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name = Producto.TODOS_PRODUCTOS, query = "select p.codigo, avg(pc.calificacion) from Producto p INNER JOIN p.calificacions pc"),
	@NamedQuery(name = Producto.TODOS_PRODUCTOS_DTO, query = "select new co.edu.uniquindio.dto.ProductoDTO(p.codigo, pc.calificacion) from Producto p INNER JOIN p.calificacions pc"),
	@NamedQuery(name = Producto.CONTAR_POR_TIPO, query = "select count(p) from Producto p group by p.tipoProducto"),
	@NamedQuery(name = Producto.PRODUCTOS_SIN_COMENTARIOS, query = "select p from Producto p where p.comentarios is empty"),
	@NamedQuery(name = Producto.PRODUCTOS_POR_USUARIO, query = "select new co.edu.uniquindio.dto.UsuarioDTO(u.cedula, u.email, count(p)) from Producto p INNER JOIN p.usuario u GROUP BY u.cedula"),
	@NamedQuery(name = Producto.REGISTROS_POR_TIPO, query = "select max(p.tipoProducto.codigo) from Producto p"),
	@NamedQuery(name = Producto.PRODUCTO_MAS_COSTOSO, query = "select p from Producto p where p.precio = (select max(p.precio) from Producto p) "),
	@NamedQuery(name = Producto.PRODUCTOS_DISPONIBLES, query = "select p from Producto p where p.disponibilidad > 0 and p.fechaLimite <= :fechaActual"),
	@NamedQuery(name = Producto.PRODUCTOS, query = "select p from Producto p"),
	@NamedQuery(name = Producto.PRODUCTOS_DISPONIBLES_CATEGORIA, query = "select p from Producto p where p.disponibilidad > 0 and p.fechaLimite <= :fechaActual group by p.tipoProducto")
})
public class Producto implements Serializable {

	@Id
	@Column(name = "CODIGO", length = 10)
	private String codigo;

	@OneToMany(mappedBy="producto")
	private ArrayList<DetalleCompra> detalleCompra;

	@OneToMany(mappedBy = "producto")
	private List<Favoritos> favoritos2;

	@OneToMany(mappedBy = "producto")
	private List<Comentario> comentarios;

	@OneToMany(mappedBy = "producto")
	private List<Calificacion> calificacions;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_LIMITE")
	private Date fechaLimite;
	
	@ElementCollection
	@Column(name = "IMAGENES")
	private List<String> imagenes;

	@Column(name = "NOMBRE")
	private String nombre;

	@Column(name = "DESCRIPCION")
	private String descripcion;

	@Column(name = "PRECIO")
	private double precio;

	@Column(name = "DISPONIBILIDAD")
	private int disponibilidad;

	@ManyToOne
	private TipoProducto tipoProducto;
	
	@ManyToOne
	private Usuario usuario;

//	private Comentario comentario;
//
//	private Calificacion calificacion;
//
//	private Favoritos favoritos;

	private static final long serialVersionUID = 1L;
	
	public static final String PRODUCTOS = "PRODUCTOS";
	
	public static final String TODOS_PRODUCTOS = "TODOS_PRODUCTOS";
	
	public static final String TODOS_PRODUCTOS_DTO = "TODOS_PRODUCTOS_DTO";
	
	public static final String TODOS_PRODUCTOS_POR_TIPO = "TODOS_PRODUCTOS_POR_TIPO";
	
	public static final String PRODUCTOS_CODIGO = "PRODUCTOS_CODIGO";

	public static final String PRODUCTOS_FECHA = "PRODUCTOS_FECHA";
	
	public static final String CONTAR_POR_TIPO = "CONTAR_POR_TIPO";
	
	public static final String PRODUCTOS_SIN_COMENTARIOS = "PRODUCTOS_SIN_COMENTARIOS";
	
	public static final String PRODUCTOS_POR_USUARIO = "PRODUCTOS_POR_USUARIO";
	
	public static final String REGISTROS_POR_TIPO ="REGISTROS_POR_TIPO";
	
	public static final String PRODUCTO_MAS_COSTOSO ="PRODUCTO_MAS_COSTOSO";
	
	public static final String PRODUCTOS_DISPONIBLES ="PRODUCTOS_DISPONIBLES";
	
	public static final String PRODUCTOS_DISPONIBLES_CATEGORIA ="PRODUCTOS_DISPONIBLES_CATEGORIA";
	
	public Producto() {
		super();
	}
	
	public Producto(Usuario vendedor) {
		super();
		
		this.usuario = vendedor;
	}

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Date getFechaLimite() {
		return this.fechaLimite;
	}

	public void setFechaLimite(Date fechaLimite) {
		this.fechaLimite = fechaLimite;
	}

	public List<String> getImagenes() {
		return imagenes;
	}

	public void setImagenes(List<String> imagenes) {
		this.imagenes = imagenes;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public double getPrecio() {
		return this.precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public int getDisponibilidad() {
		return this.disponibilidad;
	}

	public void setDisponibilidad(int disponibilidad) {
		this.disponibilidad = disponibilidad;
	}
	
	public TipoProducto getTipoProducto() {
		return tipoProducto;
	}

	public void setTipoProducto(TipoProducto tipoProducto) {
		this.tipoProducto = tipoProducto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Producto other = (Producto) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Producto [codigo=" + codigo + ", fechaLimite=" + fechaLimite + ", imagen=" + imagenes + ", nombre="
				+ nombre + ", descripcion=" + descripcion + ", precio=" + precio + ", disponibilidad=" + disponibilidad
				+ ", tipoProducto=" + tipoProducto + "]";
	}

	
}

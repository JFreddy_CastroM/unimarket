package co.edu.uniquindio.persistencia;

import java.io.Serializable;
import java.lang.String;
import java.util.ArrayList;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: TipoProducto
 *
 */

@Entity
@NamedQueries({ @NamedQuery(name = TipoProducto.LISTAR_TIPOPRODUCTOS, query = "select t from TipoProducto t") })
public class TipoProducto implements Serializable {

	@Id
	@GeneratedValue
	private int codigo;

	private String nombre;

	@OneToMany(mappedBy = "tipoProducto")
	private ArrayList<Producto> productos;

	private static final long serialVersionUID = 1L;

	public static final String LISTAR_TIPOPRODUCTOS = "LISTA_TIPOS";

	public TipoProducto() {
		super();
	}

	public int getCodigo() {
		return this.codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "TipoProducto [codigo=" + codigo + ", nombre=" + nombre + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codigo;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoProducto other = (TipoProducto) obj;
		if (codigo != other.codigo)
			return false;
		return true;
	}

	
}

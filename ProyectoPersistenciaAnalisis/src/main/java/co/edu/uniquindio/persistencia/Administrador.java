package co.edu.uniquindio.persistencia;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Administrador
 *
 */
@Entity
@DiscriminatorValue("Administrador")
@NamedQueries({
	@NamedQuery(name = Administrador.OBTENER_ADMINISTRADOR, query = "select a from Administrador a")
})
public class Administrador extends Persona implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	public static final String OBTENER_ADMINISTRADOR = "OBTENER_ADMINISTRADOR";

	public Administrador() {
		super();
	}
   
}

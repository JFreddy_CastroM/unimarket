package co.edu.uniquindio.persistencia;

import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: DetalleCompra
 *
 */
@Entity
public class DetalleCompra implements Serializable {

	@Id
	private String idCompra;
	
	@ManyToOne(targetEntity = Compra.class)
	private Compra compra;
	
	@ManyToOne(targetEntity = Producto.class)
	private Producto producto;
	
	private int cantidad;
	
	private double precio;
	
	private static final long serialVersionUID = 1L;

	public DetalleCompra() {
		super();
	}
   
}

package co.edu.uniquindio.persistencia;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Persona
 *
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@NamedQueries({
	@NamedQuery(name = Persona.TODAS_PERSONAS, query="select p from Persona p"),
	@NamedQuery(name = Persona.VERIFICAR_LOGIN, query = "select u from Persona u where u.email = :email and u.contrasena = :password"),
	@NamedQuery(name = Usuario.BUSCAR_POR_EMAIL, query = "select u from Persona u where u.email = :email"),
})
public class Persona implements Serializable {

	@Id
	@Column(name = "CEDULA", length = 29)
	private String cedula;

	@Column(name = "NOMBRE", nullable = false)
	private String nombre;

	@Column(name = "APELLIDO", nullable = false)
	private String apellido;

	@Column(name = "EMAIL")
	private String email;

	@Column(name = "TELEFONO")
	private String telefono;

	@Column(name = "DIRECCION")
	private String direccion;

	@Column(name = "CONTRASENIA")
	private String contrasena;
	
	public static final String TODAS_PERSONAS = "TODAS_PERSONAS";

	public static final String VERIFICAR_LOGIN = "VERIFICAR_LOGIN";
	
	public static final String BUSCAR_POR_EMAIL = "BUSCAR_POR_EMAIL";
	
	private static final long serialVersionUID = 1L;

	public Persona() {
		super();
	}

	public String getCedula() {
		return this.cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return this.apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contraseņa) {
		this.contrasena = contraseņa;
	}

	

	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cedula == null) ? 0 : cedula.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Persona other = (Persona) obj;
		if (cedula == null) {
			if (other.cedula != null)
				return false;
		} else if (!cedula.equals(other.cedula))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Persona [cedula=" + cedula + ", nombre=" + nombre + ", apellido=" + apellido + ", email=" + email
				+ ", telefono=" + telefono + ", direccion=" + direccion + ", contraseņa=" + contrasena + "]";
	}

	
}

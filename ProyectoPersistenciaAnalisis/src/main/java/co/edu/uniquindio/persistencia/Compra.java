package co.edu.uniquindio.persistencia;

import java.io.Serializable;
import java.lang.String;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Compra
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name=Compra.TODAS_COMPRAS, query="select c.idCompra, c.metodoPago, u.cedula, u.email from Compra c LEFT JOIN c.usuario u where c.fechaCompra = :fechaCompra"),
	@NamedQuery(name=Compra.TODAS_COMPRAS_DTO, query="select new co.edu.uniquindio.dto.CompraDTO(c.idCompra, c.metodoPago, u.cedula, u.email) from Compra c LEFT JOIN c.usuario u where c.fechaCompra = :fechaCompra"),
	@NamedQuery(name = Compra.COMPRAS_UNICAS, query = "select count(c) from Compra c")
})
public class Compra implements Serializable {

	@ManyToOne
	private Usuario usuario;

	@OneToMany(mappedBy = "compra")
	private ArrayList<DetalleCompra> detalleCompra;

	@Id
	@Column(name = "ID_COMPRA")
	private String idCompra;
	
	@Column(name = "FECHA_COMPRA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCompra;
	
	private MetodoPago metodoPago;

	public static final String TODAS_COMPRAS = "TODAS_COMPRAS";
	
	public static final String TODAS_COMPRAS_DTO = "TODAS_COMPRAS_DTO";
	
	public static final String COMPRAS_UNICAS = "COMPRAS_UNICAS";

	private static final long serialVersionUID = 1L;

	public Compra() {
		super();
	}

	public String getIdCompra() {
		return this.idCompra;
	}

	public void setIdCompra(String idCompra) {
		this.idCompra = idCompra;
	}


	public Date getFechaCompra() {
		return this.fechaCompra;
	}

	public void setFechaCompra(Date fechaCompra) {
		this.fechaCompra = fechaCompra;
	}

	public MetodoPago getMetodoPago() {
		return metodoPago;
	}

	public void setMetodoPago(MetodoPago metodoPago) {
		this.metodoPago = metodoPago;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idCompra == null) ? 0 : idCompra.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Compra other = (Compra) obj;
		if (idCompra == null) {
			if (other.idCompra != null)
				return false;
		} else if (!idCompra.equals(other.idCompra))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Compra [usuario=" + usuario + ", detalleCompra=" + detalleCompra + ", idCompra=" + idCompra
				+ ", fechaCompra=" + fechaCompra + ", metodoPago=" + metodoPago + ", productosComprados="
				+  "]";
	}

	

}

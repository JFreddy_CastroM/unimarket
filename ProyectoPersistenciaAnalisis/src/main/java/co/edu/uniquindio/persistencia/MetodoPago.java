package co.edu.uniquindio.persistencia;

public enum MetodoPago {
	CONTADO,
	CREDITO,
	TARJETA
}

package co.edu.uniquindio.persistencia;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Comentario
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name = Comentario.COMENTARIO_PRODUCTO, query = "select c from Comentario c where c.producto.codigo = :codigoProducto")
})
public class Comentario implements Serializable {

	@Id
	private String idComentario;

	@ManyToOne
	private Usuario usuario;

	@ManyToOne
	private Producto producto;

	private String comentario;

	private static final long serialVersionUID = 1L;
	
	public static final String COMENTARIO_PRODUCTO ="COMENTARIO_PRODUCTO";

	public Comentario() {
		super();
	}

	public String getComentario() {
		return this.comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	
	public String getIdComentario() {
		return idComentario;
	}

	public void setIdComentario(String idComentario) {
		this.idComentario = idComentario;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	@Override
	public String toString() {
		return "Comentario [idComentario=" + idComentario + ", usuario=" + usuario + ", producto=" + producto
				+ ", comentario=" + comentario + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idComentario == null) ? 0 : idComentario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comentario other = (Comentario) obj;
		if (idComentario == null) {
			if (other.idComentario != null)
				return false;
		} else if (!idComentario.equals(other.idComentario))
			return false;
		return true;
	}

}

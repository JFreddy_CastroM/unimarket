package co.edu.uniquindio.dto;

public class UsuarioDTO {
	private String cedula;
	private String email;
	private Long nroRegistros;
	
	public UsuarioDTO(String cedula, String email, Long nroRegistros) {
		this.cedula = cedula;
		this.email = email;
		this.nroRegistros = nroRegistros;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getNroRegistros() {
		return nroRegistros;
	}

	public void setNroRegistros(Long nroRegistros) {
		this.nroRegistros = nroRegistros;
	}

	@Override
	public String toString() {
		return "UsuarioDTO [cedula=" + cedula + ", email=" + email + ", nroRegistros=" + nroRegistros + "]";
	}
}

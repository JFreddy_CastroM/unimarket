package co.edu.uniquindio.dto;

import co.edu.uniquindio.persistencia.MetodoPago;

public class CompraDTO {
	private String idCompra;
	private MetodoPago metodoPago;
	private String cedulaUsuario;
	private String emailUsuario;
	
	public CompraDTO(String idCompra, MetodoPago metodoPago, String cedulaUsuario, String emailUsuario) {
		super();
		this.idCompra = idCompra;
		this.metodoPago = metodoPago;
		this.cedulaUsuario = cedulaUsuario;
		this.emailUsuario = emailUsuario;
	}
	
	public String getIdCompra() {
		return idCompra;
	}
	public void setIdCompra(String idCompra) {
		this.idCompra = idCompra;
	}
	public MetodoPago getMetodoPago() {
		return metodoPago;
	}
	public void setMetodoPago(MetodoPago metodoPago) {
		this.metodoPago = metodoPago;
	}
	public String getCedulaUsuario() {
		return cedulaUsuario;
	}
	public void setCedulaUsuario(String cedulaUsuario) {
		this.cedulaUsuario = cedulaUsuario;
	}
	public String getEmailUsuario() {
		return emailUsuario;
	}
	public void setEmailUsuario(String emailUsuario) {
		this.emailUsuario = emailUsuario;
	}

	@Override
	public String toString() {
		return "CompraDTO [idCompra=" + idCompra + ", metodoPago=" + metodoPago + ", cedulaUsuario=" + cedulaUsuario
				+ ", emailUsuario=" + emailUsuario + "]";
	}
	
	
}

package co.edu.uniquindio.dto;

public class ProductoDTO {
	private String codigo;
	private double calificacion;
	
	public ProductoDTO(String codigo, double calificacion) {
		this.codigo = codigo;
		this.calificacion = calificacion;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public double getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(double calificacion) {
		this.calificacion = calificacion;
	}

	@Override
	public String toString() {
		return "ProductoDTO [codigo=" + codigo + ", calificacion=" + calificacion + "]";
	}
	
	
}

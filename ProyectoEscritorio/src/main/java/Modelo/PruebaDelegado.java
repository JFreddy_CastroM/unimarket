package Modelo;

import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import Excepciones.CedulaRepetidaException;
import Excepciones.EmailRepetidoException;
import Excepciones.ProductoRepetidoException;
import co.edu.unimarket.ejb.NegocioEJBRemote;
import co.edu.uniquindio.persistencia.Comentario;
import co.edu.uniquindio.persistencia.Compra;
import co.edu.uniquindio.persistencia.Persona;
import co.edu.uniquindio.persistencia.Producto;
import co.edu.uniquindio.persistencia.TipoProducto;
import co.edu.uniquindio.persistencia.Usuario;

public class PruebaDelegado implements NegocioEJBRemote {

	public void borrarUsuario(String cedula) {
		negocioEJB.borrarUsuario(cedula);
	}

	private NegocioEJBRemote negocioEJB;

	private PruebaDelegado() {
		try {
			negocioEJB = (NegocioEJBRemote) new InitialContext().lookup(NegocioEJBRemote.JDNI);
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	public static PruebaDelegado pruebaDelegado = instancia();

	private static PruebaDelegado instancia() {
		if (pruebaDelegado == null) {
			pruebaDelegado = new PruebaDelegado();
			return pruebaDelegado;
		}
		return pruebaDelegado;
	}

	public Persona autenticarUsuario(String email, String contrasenia) {
		return negocioEJB.autenticarUsuario(email, contrasenia);
	}

	public List<Producto> listarProductosDisponibles() {
		return negocioEJB.listarProductosDisponibles();
	}

	public List<Comentario> listarComentarios(String idProducto) {
		return negocioEJB.listarComentarios(idProducto);
	}

	public void crearProducto(Producto p) throws ProductoRepetidoException {
		negocioEJB.crearProducto(p);
	}

	public void registrarUsuario(Usuario u) throws CedulaRepetidaException, EmailRepetidoException {
		negocioEJB.registrarUsuario(u);
	}

	public Producto editarProducto(Producto p) {
		return negocioEJB.editarProducto(p);
	}

	public List<Usuario> listarUsuarios() {
		return negocioEJB.listarUsuarios();
	}

	public Usuario buscarUsuarioCedula(String cedula) {
		return negocioEJB.buscarUsuarioCedula(cedula);
	}

	public Usuario editarUsuario(Usuario u) {
		return negocioEJB.editarUsuario(u);
	}

	public Producto buscarProducto(String codigo) {
		return negocioEJB.buscarProducto(codigo);
	}

	public void eliminarProducto(String codigo) {
		negocioEJB.eliminarProducto(codigo);
	}

	public List<Producto> listarProductos() {
		return negocioEJB.listarProductos();
	}

	public Persona buscarUsuario(String email) {
		return negocioEJB.buscarUsuario(email);
	}

	public String recuperarContraseņa(String email) {
		return negocioEJB.recuperarContraseņa(email);
	}

	public List<Producto> listarProductosDisponiblesCategoria() {
		return negocioEJB.listarProductosDisponiblesCategoria();
	}

	public Comentario crearComentario(String codigoProducto) {
		return negocioEJB.crearComentario(codigoProducto);
	}

	public void guardarFavorito(String codigoProducto) {
		negocioEJB.guardarFavorito(codigoProducto);
	}

	public void borarFavorito(String codigoProducto) {
		negocioEJB.borarFavorito(codigoProducto);
	}

	public Compra realizarCompra(String codigoProducto) {
		return negocioEJB.realizarCompra(codigoProducto);
	}

	public void calificarProducto(String codigoProducto) {
		negocioEJB.calificarProducto(codigoProducto);
	}

	public List<Compra> listarComprasUsuario(String cedula) {
		return negocioEJB.listarComprasUsuario(cedula);
	}

	public List<Producto> listarProductosUsuario(String cedula) {
		return negocioEJB.listarProductosUsuario(cedula);
	}
	

	@Override
	public List<TipoProducto> listarTipoProductos() {
		
		return negocioEJB.listarTipoProductos();
	}

	@Override
	public TipoProducto buscarTipoProducto(int codigo) {
		// TODO Auto-generated method stub
		return null;
	}	
	
	
	}

package Controlador;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class AdministradorController implements Initializable {

	@FXML
	private Button btnCerrarSesion;

	@FXML
	private AnchorPane content;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub

	}

	public void cerrarSesion(ActionEvent e) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/InicioSesion.fxml"));

			Parent p = loader.load();
			Scene s = new Scene(p);

			Stage primaryStage = new Stage();

			primaryStage.setScene(s);
			primaryStage.show();

			Stage administrador = (Stage) btnCerrarSesion.getScene().getWindow();
			administrador.close();

		} catch (Exception e2) {
			e2.printStackTrace();
		}
	}

	public void abrirCrearUsuario(ActionEvent e) throws Exception {

		URL myUrl = getClass().getResource("/CrearUsuario.fxml");

		content.getChildren().setAll(FXMLLoader.<Node>load(myUrl));
	}

	public void abrirListarUsuarios(ActionEvent e) throws Exception {

		URL myUrl = getClass().getResource("/ListarUsuarios.fxml");

		content.getChildren().setAll(FXMLLoader.<Node>load(myUrl));
	}
	
	public void listarProductos(ActionEvent e) throws Exception{
		
		URL myUrl = getClass().getResource("/ListarProductos.fxml");

		content.getChildren().setAll(FXMLLoader.<Node>load(myUrl));
		
	}
	
	public void recuperarContraseņa(ActionEvent e) throws Exception {
		URL myUrl = getClass().getResource("/RecuperarContraseņa.fxml");

		content.getChildren().setAll(FXMLLoader.<Node>load(myUrl));
	}
}

package Controlador;

import java.net.URL;
import java.util.ResourceBundle;

import Modelo.PruebaDelegado;
import co.edu.uniquindio.persistencia.Usuario;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class RecuperarContraseņaController implements Initializable {
	
	private PruebaDelegado pruebaDelegado;
	
	@FXML
	private Label lblContraseņa;
	
	@FXML
	private TextField txtEmail;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		pruebaDelegado = PruebaDelegado.pruebaDelegado;
	}

	public void recuperarContraseņa(ActionEvent e) {
		String email = txtEmail.getText();
		
		String contraseņa = pruebaDelegado.recuperarContraseņa(email);
		
		lblContraseņa.setText(contraseņa);
	}
}

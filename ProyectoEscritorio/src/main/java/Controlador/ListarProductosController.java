package Controlador;

import java.net.URL;
import java.util.ResourceBundle;

import Modelo.PruebaDelegado;
import co.edu.uniquindio.persistencia.Producto;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

public class ListarProductosController implements Initializable {

	private PruebaDelegado pruebaDelegado;

	private ObservableList<String> items;

	@FXML
	private ListView<String> listProductos;

	@FXML
	private TextField txtCodigo;

	@FXML
	private TextField txtNombre;

	@FXML
	private TextField txtPrecio;

	@FXML
	private TextField txtDisponibilidad;

	@FXML
	private TextField txtFechaLimite;

	@FXML
	private TextField txtTipoProducto;

	@FXML
	private TextArea txtDescripcion;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		txtCodigo.setEditable(false);
		txtNombre.setEditable(false);
		txtPrecio.setEditable(false);
		txtDisponibilidad.setEditable(false);
		txtFechaLimite.setEditable(false);
		txtTipoProducto.setEditable(false);
		txtDescripcion.setEditable(false);
		
		pruebaDelegado = PruebaDelegado.pruebaDelegado;

		items = FXCollections.observableArrayList();
		
		for (Producto p : pruebaDelegado.listarProductos()) {
			items.add(p.getCodigo());
		}
		listProductos.setItems(items);
		
		listProductos.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if (event.getButton().equals(MouseButton.PRIMARY) && event.getClickCount() == 2) {
					
					String cProducto = listProductos.getSelectionModel().getSelectedItem();
					
					Producto p = pruebaDelegado.buscarProducto(cProducto);
					
					txtCodigo.setText(p.getCodigo());
					txtNombre.setText(p.getNombre());
					txtPrecio.setText(p.getPrecio() + "");
					txtDisponibilidad.setText(p.getDisponibilidad() + "");
					txtFechaLimite.setText(p.getFechaLimite() + "");
					txtTipoProducto.setText(p.getTipoProducto() + "");
					txtDescripcion.setText(p.getDescripcion());
				}
			}
		});
	}

}

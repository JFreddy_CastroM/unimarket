package Controlador;

import java.net.URL;
import java.util.ResourceBundle;

import Modelo.PruebaDelegado;
import co.edu.uniquindio.persistencia.Administrador;
import co.edu.uniquindio.persistencia.Persona;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class InicioSesionController implements Initializable {
	private PruebaDelegado pruebaDelegado;
	@FXML
	private TextField txtEmail;
	@FXML
	private PasswordField txtContrase�a;

	@FXML
	private Button btnIniciar;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		pruebaDelegado = PruebaDelegado.pruebaDelegado;
	}

	@FXML
	public void validarUsuario(ActionEvent e) {
		String email = txtEmail.getText();
		String clave = txtContrase�a.getText();

		if (!email.isEmpty() && !clave.isEmpty()) {
			Persona p = pruebaDelegado.autenticarUsuario(email, clave);
			boolean aux = p instanceof Administrador;
			if (p == null) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error!!!!!!!");
				alert.setHeaderText(null);
				alert.setContentText("Correo o Contrase�a incorrectos");

				alert.showAndWait();
			} else {
				if (aux) {
					try {
						FXMLLoader loader = new FXMLLoader();
						loader.setLocation(getClass().getResource("/Administrador.fxml"));

						Parent parent = loader.load();
						Scene s = new Scene(parent);

						Stage primaryStage = new Stage();

						primaryStage.setScene(s);
						primaryStage.show();

						Stage principal = (Stage) btnIniciar.getScene().getWindow();
						principal.close();

					} catch (Exception ex) {
						ex.printStackTrace();
					}

				} else {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Error!!!!!!!");
					alert.setHeaderText(null);
					alert.setContentText("Solo puede iniciar sesion el administrador");

					alert.showAndWait();
				}
			}
		} else {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error!!!!!!!");
			alert.setHeaderText(null);
			alert.setContentText("Llene todos los campos por favor");

			alert.showAndWait();
		}
	}

	public void recuperarContrase�a(ActionEvent e) {
		String email = txtEmail.getText();

		String contrase�a = pruebaDelegado.recuperarContrase�a(email);

		txtContrase�a.setText(contrase�a);
	}
}

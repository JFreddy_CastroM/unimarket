package Controlador;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import Modelo.PruebaDelegado;
import co.edu.uniquindio.persistencia.Usuario;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

public class ListarUsuariosController implements Initializable {

	private PruebaDelegado pruebaDelegado;

	private ObservableList<String> items;

	@FXML
	private ListView<String> listUsuarios;

	@FXML
	private TextField lblNombre;

	@FXML
	private TextField lblApellido;

	@FXML
	private TextField lblCedula;

	@FXML
	private TextField lblEmail;

	@FXML
	private TextField lblTelefono;

	@FXML
	private TextField lblDireccion;

	@FXML
	private TextField lblContraseņa;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		lblCedula.setDisable(true);

		pruebaDelegado = PruebaDelegado.pruebaDelegado;

		items = FXCollections.observableArrayList();

		for (Usuario u : pruebaDelegado.listarUsuarios()) {
			items.add(u.getCedula());
		}
		listUsuarios.setItems(items);

		listUsuarios.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if (event.getButton().equals(MouseButton.PRIMARY) && event.getClickCount() == 2) {

					String usuario = listUsuarios.getSelectionModel().getSelectedItem();

					Usuario u = pruebaDelegado.buscarUsuarioCedula(usuario);

					lblNombre.setText(u.getNombre());
					lblApellido.setText(u.getApellido());
					lblCedula.setText(u.getCedula());
					lblEmail.setText(u.getEmail());
					lblTelefono.setText(u.getTelefono());
					lblDireccion.setText(u.getDireccion());
					lblContraseņa.setText(u.getContrasena());
				}
			}
		});
	}

	public void eliminarUsuario(ActionEvent e) {
		if (listUsuarios.getSelectionModel().getSelectedIndex() < 0) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Debe seleccionar un usuario a eliminar");

			alert.showAndWait();
		} else {
			String usuario = listUsuarios.getSelectionModel().getSelectedItem();
			int index = listUsuarios.getSelectionModel().getSelectedIndex();

			pruebaDelegado.borrarUsuario(usuario);

			items.remove(index);
			
			Alert alerta = new Alert(AlertType.CONFIRMATION);
			alerta.setTitle("Exito");
			alerta.setHeaderText(null);
			alerta.setContentText("Usuario eliminado correctamente");

			alerta.showAndWait();
		}
	}

	public void actualizarUsuario(ActionEvent e) {
		String usuario = listUsuarios.getSelectionModel().getSelectedItem();
		int index = listUsuarios.getSelectionModel().getSelectedIndex();

		Usuario u = pruebaDelegado.buscarUsuarioCedula(usuario);
		
		u.setNombre(lblNombre.getText());
		u.setApellido(lblApellido.getText());
		u.setEmail(lblEmail.getText());
		u.setTelefono(lblTelefono.getText());
		u.setDireccion(lblDireccion.getText());
		u.setContrasena(lblContraseņa.getText());
		
		Usuario uActualizado = pruebaDelegado.editarUsuario(u);
		
		items.remove(index);
		items.add(uActualizado.getCedula());
		
		listUsuarios.refresh();
		
		Alert alerta = new Alert(AlertType.CONFIRMATION);
		alerta.setTitle("Exito");
		alerta.setHeaderText(null);
		alerta.setContentText("Usuario actualizado correctamente");

		alerta.showAndWait();
	}
}

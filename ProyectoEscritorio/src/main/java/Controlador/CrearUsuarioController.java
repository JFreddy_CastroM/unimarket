package Controlador;

import java.net.URL;
import java.util.ResourceBundle;

import Excepciones.CedulaRepetidaException;
import Excepciones.EmailRepetidoException;
import Modelo.PruebaDelegado;
import co.edu.uniquindio.persistencia.Usuario;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;

public class CrearUsuarioController implements Initializable {
	
	@FXML
	private TextField txtCedula;
	
	@FXML
	private TextField txtNombre;
	
	@FXML
	private TextField txtApellido;
	
	@FXML
	private TextField txtEmail;
	
	@FXML
	private TextField txtDireccion;
	
	@FXML
	private TextField txtTelefono;
	
	@FXML
	private TextField txtClave;
	
	private PruebaDelegado pruebaDelegado;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		pruebaDelegado = PruebaDelegado.pruebaDelegado;
	}

	public void crearUsuario(ActionEvent e) {
		
		String cedula = txtCedula.getText();
		String nombre = txtNombre.getText();
		String apellido = txtApellido.getText();
		String email = txtEmail.getText();
		String direccion = txtDireccion.getText();
		String telefono = txtTelefono.getText();
		String contraseņa = txtClave.getText();
		
		Usuario u = new Usuario();
		
		u.setCedula(cedula);
		u.setNombre(nombre);
		u.setApellido(apellido);
		u.setEmail(email);
		u.setDireccion(direccion);
		u.setTelefono(telefono);
		u.setContrasena(contraseņa);
		
		try {
			pruebaDelegado.registrarUsuario(u);
			
			Alert alerta = new Alert(AlertType.CONFIRMATION);
			alerta.setTitle("Exito");
			alerta.setHeaderText(null);
			alerta.setContentText("Usuario creado correctamente");
			
			txtCedula.clear();
			txtNombre.clear();
			txtApellido.clear();
			txtEmail.clear();
			txtDireccion.clear();
			txtTelefono.clear();
			txtClave.clear();

			alerta.showAndWait();
		} catch (CedulaRepetidaException | EmailRepetidoException e1) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText(e1.getMessage());

			alert.showAndWait();
		}
	}
}

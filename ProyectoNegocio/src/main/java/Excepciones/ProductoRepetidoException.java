package Excepciones;

public class ProductoRepetidoException extends Exception
{
	public ProductoRepetidoException(String message) {
		super(message);
	}
}

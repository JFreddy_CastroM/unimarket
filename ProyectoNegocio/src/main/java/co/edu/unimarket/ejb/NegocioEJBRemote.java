package co.edu.unimarket.ejb;

import java.util.List;

import javax.ejb.Remote;

import Excepciones.CedulaRepetidaException;
import Excepciones.EmailRepetidoException;
import Excepciones.ProductoRepetidoException;
import co.edu.uniquindio.persistencia.Comentario;
import co.edu.uniquindio.persistencia.Compra;
import co.edu.uniquindio.persistencia.Persona;
import co.edu.uniquindio.persistencia.Producto;
import co.edu.uniquindio.persistencia.TipoProducto;
import co.edu.uniquindio.persistencia.Usuario;

@Remote
public interface NegocioEJBRemote {

	String JDNI = "java:global/ProyectoEAR/ProyectoNegocio/NegocioEJB!co.edu.unimarket.ejb.NegocioEJBRemote";
			
	Persona autenticarUsuario(String email, String contrasenia);

	List<Producto> listarProductosDisponibles();

	List<Comentario> listarComentarios(String idProducto);

	void crearProducto(Producto p) throws ProductoRepetidoException;

	void registrarUsuario(Usuario u) throws CedulaRepetidaException, EmailRepetidoException;

	Producto editarProducto(Producto p);
	
	List<Usuario> listarUsuarios();
	
	Usuario buscarUsuarioCedula(String cedula);
	
	void borrarUsuario(String cedula);
	
	Usuario editarUsuario(Usuario u);
	
	Producto buscarProducto( String codigo );
	
	void eliminarProducto(String codigo);
	
	List<Producto> listarProductos();
	
	Persona buscarUsuario(String email);
	
	String recuperarContraseņa(String email);
	
	List<Producto> listarProductosDisponiblesCategoria();
	
	Comentario crearComentario(String codigoProducto);
	
	void guardarFavorito(String codigoProducto);
	
	void borarFavorito(String codigoProducto);
	
	Compra realizarCompra(String codigoProducto);
	
	void calificarProducto(String codigoProducto);
	
	List<Compra> listarComprasUsuario(String cedula);
	
	List<Producto> listarProductosUsuario(String cedula);

	List<TipoProducto> listarTipoProductos();
	
	TipoProducto buscarTipoProducto(int codigo);
}

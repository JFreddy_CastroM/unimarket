package co.edu.unimarket.ejb;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import Excepciones.CedulaRepetidaException;
import Excepciones.EmailRepetidoException;
import Excepciones.ProductoRepetidoException;
import co.edu.uniquindio.persistencia.Comentario;
import co.edu.uniquindio.persistencia.Compra;
import co.edu.uniquindio.persistencia.Persona;
import co.edu.uniquindio.persistencia.Producto;
import co.edu.uniquindio.persistencia.TipoProducto;
import co.edu.uniquindio.persistencia.Usuario;

/**
 * Session Bean implementation class NegocioEJB
 */
@Stateless
@LocalBean
public class NegocioEJB implements NegocioEJBRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Default constructor.
	 */
	public NegocioEJB() {

	}

	@Override
	public Persona autenticarUsuario(String email, String contrasenia) {

		TypedQuery<Persona> q = entityManager.createNamedQuery(Persona.VERIFICAR_LOGIN, Persona.class);

		q.setParameter("email", email);
		q.setParameter("password", contrasenia);

		List<Persona> l = q.getResultList();

		if (!l.isEmpty()) {
			Persona p = l.get(0);
			System.out.println(p);
			return p;
		}

		return null;
	}

	@Override
	public List<Producto> listarProductosDisponibles() {
		TypedQuery<Producto> q = entityManager.createNamedQuery(Producto.PRODUCTOS_DISPONIBLES, Producto.class);

		q.setParameter("fechaActual", new Date());

		List<Producto> l = q.getResultList();

		return l;
	}

	@Override
	public List<Comentario> listarComentarios(String idProducto) {
		TypedQuery<Comentario> q = entityManager.createNamedQuery(Comentario.COMENTARIO_PRODUCTO, Comentario.class);

		q.setParameter("codigoProducto", idProducto);

		List<Comentario> l = q.getResultList();

		return l;
	}

	@Override
	public void crearProducto(Producto p) throws ProductoRepetidoException {

		if (entityManager.find(Producto.class, p.getCodigo()) != null) {
			throw new ProductoRepetidoException("El producto ya existe");
		}

		entityManager.persist(p);
	}

	@Override
	public void registrarUsuario(Usuario u) throws CedulaRepetidaException, EmailRepetidoException {

		if (entityManager.find(Usuario.class, u.getCedula()) != null) {
			throw new CedulaRepetidaException("La cedula del usuario ya existe");
		}

		if (buscarUsuario(u.getEmail()) != null) {
			throw new EmailRepetidoException("El email ya se encuentra en uso");
		}

		entityManager.persist(u);
	}

	@Override
	public Producto editarProducto(Producto p) {

		Producto actualizar = entityManager.find(Producto.class, p.getCodigo());

		actualizar.setDescripcion(p.getDescripcion());
		actualizar.setDisponibilidad(p.getDisponibilidad());
		actualizar.setFechaLimite(p.getFechaLimite());
		actualizar.setNombre(p.getNombre());
		actualizar.setPrecio(p.getPrecio());

		entityManager.merge(actualizar);

		return actualizar;
	}

	@Override
	public Persona buscarUsuario(String email) {
		TypedQuery<Persona> q = entityManager.createNamedQuery(Persona.BUSCAR_POR_EMAIL, Persona.class);

		q.setParameter("email", email);

		List<Persona> l = q.getResultList();
		
		if (l.isEmpty()) {
			return null;
		}

		return l.get(0);
	}

	@Override
	public List<Usuario> listarUsuarios() {
		TypedQuery<Usuario> q = entityManager.createNamedQuery(Usuario.LISTAR_USUARIOS, Usuario.class);

		List<Usuario> l = q.getResultList();

		return l;
	}
	
	@Override
	public List<TipoProducto> listarTipoProductos() {
		TypedQuery<TipoProducto> q = entityManager.createNamedQuery(TipoProducto.LISTAR_TIPOPRODUCTOS, TipoProducto.class);

		List<TipoProducto> l = q.getResultList();
		return l;
	}

	@Override
	public Usuario buscarUsuarioCedula(String cedula) {
		TypedQuery<Usuario> q = entityManager.createNamedQuery(Usuario.BUSCAR_USUARIO_CEDULA, Usuario.class);

		q.setParameter("cedula", cedula);

		List<Usuario> l = q.getResultList();

		return l.get(0);
	}

	@Override
	public void borrarUsuario(String cedula) {
		Usuario u = entityManager.find(Usuario.class, cedula);
		Persona p = entityManager.find(Persona.class, cedula);

		entityManager.remove(u);
		entityManager.remove(p);
	}

	@Override
	public Usuario editarUsuario(Usuario u) {
		Usuario actualizar = entityManager.find(Usuario.class, u.getCedula());

		actualizar.setNombre(u.getNombre());
		actualizar.setApellido(u.getApellido());
		actualizar.setEmail(u.getEmail());
		actualizar.setDireccion(u.getDireccion());
		actualizar.setTelefono(u.getTelefono());
		actualizar.setContrasena(u.getContrasena());

		entityManager.merge(actualizar);

		return actualizar;
	}

	@Override
	public Producto buscarProducto(String codigo) {

		Producto p = entityManager.find(Producto.class, codigo);

		return p;
	}

	@Override
	public void eliminarProducto(String codigo) {
		Producto p = entityManager.find(Producto.class, codigo);
		
		entityManager.remove(p);
	}

	@Override
	public List<Producto> listarProductos() {
		
		TypedQuery<Producto> q = entityManager.createNamedQuery(Producto.PRODUCTOS, Producto.class);
		
		List<Producto> l = q.getResultList();
		
		return l;
	}

	@Override
	public String recuperarContraseņa(String email) {

		Persona u = buscarUsuario(email);
		
		String contraseņa = u.getContrasena();
		
		return contraseņa;
	}

	@Override
	public List<Producto> listarProductosDisponiblesCategoria() {
		TypedQuery<Producto> q  = entityManager.createNamedQuery(Producto.PRODUCTOS_DISPONIBLES_CATEGORIA, Producto.class);
		
		List<Producto> l = q.getResultList();
		
		return l;
	}

	@Override
	public Comentario crearComentario(String codigoProducto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void guardarFavorito(String codigoProducto) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void borarFavorito(String codigoProducto) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Compra realizarCompra(String codigoProducto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void calificarProducto(String codigoProducto) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Compra> listarComprasUsuario(String cedula) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Producto> listarProductosUsuario(String cedula) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TipoProducto buscarTipoProducto(int codigo) {
		TipoProducto tp = entityManager.find(TipoProducto.class, codigo);
		
		return tp;
	}

}
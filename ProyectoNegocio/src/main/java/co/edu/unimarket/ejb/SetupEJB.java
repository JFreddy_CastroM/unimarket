package co.edu.unimarket.ejb;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import co.edu.uniquindio.persistencia.Administrador;
import co.edu.uniquindio.persistencia.Producto;
import co.edu.uniquindio.persistencia.TipoProducto;
import co.edu.uniquindio.persistencia.Usuario;

/**
 * Session Bean implementation class SetupEJB
 */
@Singleton
@LocalBean
@Startup
public class SetupEJB {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Default constructor.
	 */
	public SetupEJB() {

	}

	@PostConstruct
	public void config() {

		Query q = entityManager.createNamedQuery(Administrador.OBTENER_ADMINISTRADOR);

		if (q.getResultList().isEmpty()) {
			Administrador u = new Administrador();
			u.setNombre("Hector");
			u.setApellido("Perez");
			u.setCedula("30091946");
			u.setContrasena("1234567890");
			u.setDireccion("Ponce - PR");
			u.setEmail("lavoe@email.com");
			u.setTelefono("29061993");

			entityManager.persist(u);
			
			Usuario u1 = new Usuario();
			u1.setNombre("Hector Juan");
			u1.setApellido("Perez Martinez");
			u1.setCedula("40091946");
			u1.setContrasena("1234567890");
			u1.setDireccion("Ponce - PR");
			u1.setEmail("lavoePR@email.com");
			u1.setTelefono("29061993");
			
			entityManager.persist(u1);

			TipoProducto tp1 = new TipoProducto();
			tp1.setNombre("DEPORTE");

			entityManager.persist(tp1);
			
			TipoProducto tp2 = new TipoProducto();
			tp2.setNombre("TECNOLOGIA");

			entityManager.persist(tp2);
			
			TipoProducto tp3 = new TipoProducto();
			tp3.setNombre("LIBROS");

			entityManager.persist(tp3);

		}
	}

}

package co.edu.uniquindio.beans;

import javax.annotation.ManagedBean;
import javax.enterprise.context.ApplicationScoped;

@ManagedBean
public class HolaMundoBean {
	
	private String mensaje = "Hola Texto Bean";

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
}

package co.edu.uniquindio.beans;

import javax.annotation.ManagedBean;
import javax.enterprise.context.ApplicationScoped;

@ManagedBean
public class EjemploBean {
	
	private String atributo1;
	private String atributo2;
	
	public String getAtributo1() {
		return atributo1;
	}
	public void setAtributo1(String atributo1) {
		this.atributo1 = atributo1;
	}
	public String getAtributo2() {
		return atributo2;
	}
	public void setAtributo2(String atributo2) {
		this.atributo2 = atributo2;
	}
	
	public void cambiar() {
		String temp = atributo1;
		atributo1 = atributo2;
		atributo2 = temp;
	}
}

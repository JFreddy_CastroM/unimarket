package co.edu.uniquindio.beans;

import javax.annotation.ManagedBean;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;
import javax.swing.JOptionPane;

import Excepciones.CedulaRepetidaException;
import Excepciones.EmailRepetidoException;
import co.edu.unimarket.ejb.NegocioEJB;
import co.edu.uniquindio.persistencia.Usuario;

@ManagedBean
public class UsuarioBean {
	
	private String cedula, nombre, apellido, email, direccion, contraseña, telefono;
	
	@EJB
	private NegocioEJB negocioEJB;
	
	public String registrarUsuario() {
		
		Usuario u = new Usuario();
		
		u.setCedula(cedula);
		u.setNombre(nombre);
		u.setApellido(apellido);
		u.setDireccion(direccion);
		u.setTelefono(telefono);
		u.setContrasena(contraseña);
		u.setEmail(email);
		
		try {
			negocioEJB.registrarUsuario(u);
			
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro Exitoso", "Registro Exitoso");
			FacesContext.getCurrentInstance().addMessage(null, message);
			
		} catch (CedulaRepetidaException | EmailRepetidoException e) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage());
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
		
		return null;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getContraseña() {
		return contraseña;
	}

	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
}

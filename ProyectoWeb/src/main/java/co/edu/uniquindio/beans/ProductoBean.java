package co.edu.uniquindio.beans;

import java.util.Date;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;
import Excepciones.ProductoRepetidoException;
import co.edu.unimarket.ejb.NegocioEJB;
import co.edu.uniquindio.persistencia.Producto;
import co.edu.uniquindio.persistencia.TipoProducto;
import co.edu.uniquindio.persistencia.Usuario;

@ManagedBean
public class ProductoBean {

	private String codigo, nombre, descripcion;
	private String precio;
	private String disponibilidad;
	private TipoProducto tipoProducto;
	private Date fechaLimite;
	private Usuario usuario;
	private List<TipoProducto> listaTipos;
	private List<Producto> productos;

	@EJB
	private NegocioEJB negocioEJB;
	
	@PostConstruct
	public void inicializar() {
		this.productos  = negocioEJB.listarProductos();
		this.listaTipos = negocioEJB.listarTipoProductos();
		System.out.println(productos);
		System.out.println(listaTipos);
	}

	public String crearProducto() {
		try {
			
			double precio = Double.parseDouble(this.precio);
			int disponibilidad = Integer.parseInt(this.disponibilidad);
			
			Usuario vendedor = (Usuario) negocioEJB.buscarUsuario("lavoePR@email.com");

			Producto p = new Producto(vendedor);

			p.setCodigo(codigo);
			p.setNombre(nombre);
			p.setDescripcion(descripcion);
			p.setDisponibilidad(disponibilidad);
			p.setFechaLimite(fechaLimite);
			p.setPrecio(precio);
			p.setTipoProducto(tipoProducto);

			negocioEJB.crearProducto(p);

			return "productoCreado";

		} catch (ProductoRepetidoException e) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage());
			FacesContext.getCurrentInstance().addMessage(null, message);
		}

		return null;
	}
	
	

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getPrecio() {
		return precio;
	}

	public void setPrecio(String precio) {
		this.precio = precio;
	}

	public String getDisponibilidad() {
		return disponibilidad;
	}

	public void setDisponibilidad(String disponibilidad) {
		this.disponibilidad = disponibilidad;
	}

	public TipoProducto getTipoProducto() {
		return tipoProducto;
	}

	public void setTipoProducto(TipoProducto tipoProducto) {
		this.tipoProducto = tipoProducto;
	}

	public Date getFechaLimite() {
		return fechaLimite;
	}

	public void setFechaLimite(Date fechaLimite) {
		this.fechaLimite = fechaLimite;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<TipoProducto> getListaTipos() {
		return listaTipos;
	}

	public void setListaTipos(List<TipoProducto> listaTipos) {
		this.listaTipos = listaTipos;
	}

	public List<Producto> getProductos() {
		return productos;
	}

	public void setProductos(List<Producto> productos) {
		this.productos = productos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tipoProducto == null) ? 0 : tipoProducto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductoBean other = (ProductoBean) obj;
		if (tipoProducto == null) {
			if (other.tipoProducto != null)
				return false;
		} else if (!tipoProducto.equals(other.tipoProducto))
			return false;
		return true;
	}

	
}

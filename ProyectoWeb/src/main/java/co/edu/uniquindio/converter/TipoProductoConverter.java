package co.edu.uniquindio.converter;

import javax.annotation.ManagedBean;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.annotation.FacesConfig;
import javax.faces.annotation.FacesConfig.Version;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Named;

import co.edu.unimarket.ejb.NegocioEJB;
import co.edu.uniquindio.persistencia.TipoProducto;

@FacesConverter(value = "tipoProductoConverter")
public class TipoProductoConverter implements Converter<TipoProducto> {

	@EJB
	private NegocioEJB negocioEJB;

	@Override
	public TipoProducto getAsObject(FacesContext context, UIComponent component, String value) {

		if (value != null && !"".equals(value)) {
			try {
				TipoProducto tp = negocioEJB.buscarTipoProducto(Integer.parseInt(value));
				return tp;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, TipoProducto value) {
		if (value != null) {
			return "" + value.getCodigo();
		}

		return null;
	}

}
